package nl.bioinf.bengels.wrapper_bengels;

import weka.classifiers.trees.RandomForest;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

import java.io.IOException;

public class WekaRunner {
    public static void main(String[] args) throws Exception {
        WekaRunner wekaRunner = new WekaRunner();
        wekaRunner.start();
    }

    private void start() {
        String unknownData = "data/unknown_bcell.arff";
        try {
            RandomForest forest = loadClassifier();
            Instances unknownInstances = loadArff(unknownData);
            // System.out.println("\nUnknown instances \n" + unknownInstances);
            classifyNewInstance(forest, unknownInstances);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void classifyNewInstance(RandomForest forest, Instances unknownInstances) throws Exception {
        Instances labeledInstances = new Instances(unknownInstances);
        for (int i = 0; i < unknownInstances.numInstances(); i++){
            double classLabel = forest.classifyInstance(unknownInstances.instance(i));
            labeledInstances.instance(i).setClassValue(classLabel);
        }
        System.out.println("\nNew labeled instances \n" + labeledInstances);
    }

    private RandomForest loadClassifier() throws Exception{
        String modelFile = "data/RandomForest.model";
        return (RandomForest) weka.core.SerializationHelper.read(modelFile);
    }

    private Instances loadArff(String datafile) throws IOException {
        try {
            DataSource source = new DataSource(datafile);
            Instances data = source.getDataSet();
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() -1);
            return data;
        } catch (Exception e){
            throw new IOException("could not read from file");
        }
    }
}
