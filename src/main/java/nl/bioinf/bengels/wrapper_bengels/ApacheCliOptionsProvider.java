package nl.bioinf.bengels.wrapper_bengels;

import org.apache.commons.cli.*;

public class ApacheCliOptionsProvider {
    private static final String FILENAME = "filename";

    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;
    private String fileName;

    public ApacheCliOptionsProvider (final String[] args){
        this.clArguments = args;
        initialize();
    }

    private void initialize(){
        buildOptions();
        processCommandLine();
    }

    private void buildOptions() {
        this.options = new Options();
        Option fileOption = new Option("f", FILENAME, true, "give file with unkown instances");
        options.addOption(fileOption);
    }

    private void processCommandLine() {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);
            fileName = this.commandLine.getOptionValue(FILENAME).trim();
        } catch ( ParseException ex) {
          throw new IllegalArgumentException(ex);
        }
    }

    public String getFilename () {
        return fileName;
    }
}
