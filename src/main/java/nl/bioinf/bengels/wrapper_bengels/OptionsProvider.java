package nl.bioinf.bengels.wrapper_bengels;

public interface OptionsProvider {
    String getFileName();
}
