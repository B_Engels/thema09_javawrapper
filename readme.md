title: Java Wrapper

author: Bart Engels

date: 17-01-2021

#################################################################################################

This project contains a java wrapper that can predict if a bcell has antibody inducing activity.

In the data directory there is a unkown_bcell.arff file, here are 3 unknown instances.
When you run the main of WekaRunner.java these instances will be classified. Where true means they have antibody inducing acitivity and false means they don't.

#################################################################################################

How to use this wrapper:

step1:

Clone this wrapper by copping this link in to the terminal: git clone https://B_Engels@bitbucket.org/B_Engels/thema09_javawrapper.git

Step2: open it in a interpreter for example IntalliJ. 

Step3: run the WekaRunner.java

#################################################################################################

Questions?

B.engels@st.hanze.nl
